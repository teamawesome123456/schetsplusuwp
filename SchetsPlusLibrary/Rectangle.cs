﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;

namespace SchetsPlusLibrary
{
    class Rectangle : Shape
    {
        public override string Export()
        {
            return $"<rect width=\"{Size.Width}\" height=\"{Size.Height}\" style=\"fill:{colorToString(FillColor)};stroke-width:{BorderWidth};stroke:{colorToString(BorderColor)}\" />";
        }
    }
}
