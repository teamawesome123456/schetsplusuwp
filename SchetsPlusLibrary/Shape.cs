﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.UI;

namespace SchetsPlusLibrary
{
    public abstract class Shape
    {
        public Size Size { get; set; }
        public Point Location { get; set; }
        public Color FillColor { get; set; }
        public Color BorderColor { get; set; }
        public float BorderWidth { get; set; }
        public abstract string Export();
        
        protected string colorToString(Color c)
        {
            return $"rgb({c.R},{c.G},{c.B})";
        }
    }
}
